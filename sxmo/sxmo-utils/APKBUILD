# Maintainer: Miles Alan <m@milesalan.com>
pkgname=sxmo-utils
pkgver=1.4.1
pkgrel=0
pkgdesc="Utility scripts, programs, and configs that hold the Sxmo UI environment together"
url="https://git.sr.ht/~mil/sxmo-utils"
arch="all"
license="MIT"
makedepends="libx11-dev xproto linux-headers"
depends="
	sxmo-dmenu
	sxmo-dwm
	sxmo-st
	sxmo-surf
	svkbd
	lisgd
	clickclack

	alsa-utils
	autocutsel
	codemadness-frontends
	conky
	coreutils
	curl
	dunst
	ffmpeg
	gawk
	geoclue
	grep
	inotify-tools
	mediainfo
	modemmanager
	mpv
	ncurses
	sfeed
	sxiv
	terminus-font
	font-terminus-nerd
	font-fira-mono-nerd
	tzdata
	v4l-utils
	vis
	w3m
	xclip
	xdotool
	xdpyinfo
	xinput
	xprop
	xrandr
	xrdb
	xsel
	xset
	xsetroot
	xwininfo
	youtube-dl
"

options="!check" # has no tests
subpackages="$pkgname-openrc"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~mil/sxmo-utils/archive/$pkgver.tar.gz"
install="$pkgname.post-install $pkgname.pre-deinstall"

package() {
	mkdir -p "$pkgdir/etc/modules-load.d/"
	printf %b "snd-aloop" > "$pkgdir/etc/modules-load.d/sxmo.conf"
	mkdir -p "$pkgdir/etc/modprobe.d/"
	printf %b "options snd slots=,snd-aloop" > "$pkgdir/etc/modprobe.d/sxmo.conf"

	make  -C "$builddir" DESTDIR=$pkgdir install
}

sha512sums="b80ed389e29404421a829bba9b76814ee3afc3c11527d2f3e87f95eb83bbc498ead43e38c0d6164c4336c2d5633af569c43bcdb28c0f0d886e00ee506184b537  sxmo-utils-1.4.1.tar.gz"
